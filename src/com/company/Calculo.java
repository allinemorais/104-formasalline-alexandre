package com.company;

import java.util.ArrayList;

public class Calculo implements ICalculo {

    public  Calculo(ArrayList<Integer> lista ) {

        if (lista.size() == 1) {
            this.calcular(lista.get(0));
        }
           else if(lista.size() == 2)
           {
               this.calcular(lista.get(0),lista.get(1));
           }
           else
               {
                   this.calcular(lista.get(0),lista.get(1),lista.get(2));
               }

    }

    @java.lang.Override
    public void calcular(int lado1) {
        Circulo circulo = new Circulo(lado1);
        ImprirmirResultado.ImprirmirResultado(circulo.calcular(), circulo.getNomeForma());

    }

    @java.lang.Override
    public void calcular(int lado1, int lado2) {
        Retangulo retangulo = new Retangulo(lado1, lado2);
        ImprirmirResultado.ImprirmirResultado(retangulo.calcular(), retangulo.getNomeForma());

    }

    @java.lang.Override
    public void calcular(int lado1, int lado2, int lado3) {
        Triangulo triangulo = new Triangulo(lado1, lado2,lado3);
        if(triangulo.validar()) {
            ImprirmirResultado.ImprirmirResultado(triangulo.calcular(), triangulo.getNomeForma());
        }else
            {
                System.out.println("Formulário Inválido");
            }
    }
}
