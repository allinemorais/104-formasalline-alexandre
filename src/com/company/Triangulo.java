package com.company;

public class Triangulo extends Formas {


    public Triangulo(int lado1, int lado2, int lado3) {

        this.setLado1(lado1);
        this.setLado2(lado2);
        this.setLado3(lado3);
        this.setNomeForma("Triangulo");
 }


    public boolean validar()
    {
        if ((this.getLado1() + this.getLado2()) > this.getLado3() && (this.getLado1() + this.getLado3()) > this.getLado2() &&
                (this.getLado2() + this.getLado3()) > this.getLado1())
            return true;

      return  false;

    }

    public double calcular()
    {
        double area = ((this.getLado1() + this.getLado2() + this.getLado3()) / 2);
        return Math.sqrt(area *(area - this.getLado1() )* (area - this.getLado2()) * (area - this.getLado3()));

    }

}
