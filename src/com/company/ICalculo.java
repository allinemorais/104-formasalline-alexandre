package com.company;

public interface ICalculo {
    public void calcular(int lado1);
    public void calcular(int lado1, int lado2);
    public void calcular(int lado1, int lado2, int lado3);
}
