package com.company;

public class Retangulo extends Formas
{

    public Retangulo( int lado1, int lado2) {

        this.setLado1(lado1);
        this.setLado2(lado2);

        setNomeForma("Retangulo");
    }

    public double calcular()
    {
        return (this.getLado1() * this.getLado2());
    }
}
