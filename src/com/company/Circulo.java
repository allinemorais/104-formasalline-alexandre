package com.company;


public class Circulo  extends Formas {

    public Circulo(int lados) {

        this.setNomeForma("Circulo");
        this.setLado1(lados);
    }

    public double calcular()
    {
       return Math.pow(this.getLado1(),2)*Math.PI;

    }



}
